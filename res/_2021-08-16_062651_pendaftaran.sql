-- MySQL dump 10.13  Distrib 5.7.32, for osx10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: pendaftaran
-- ------------------------------------------------------
-- Server version	5.7.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pendaftaran_mahasiswa`
--

DROP TABLE IF EXISTS `pendaftaran_mahasiswa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pendaftaran_mahasiswa` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `nama` varchar(255) NOT NULL COMMENT 'Nama maba',
  `alamat` varchar(255) NOT NULL COMMENT 'alamat maba',
  `jenis_kelamin` varchar(255) NOT NULL COMMENT 'jenis kelamin maba',
  `agama` varchar(255) NOT NULL COMMENT 'agama maba',
  `sekolah_asal` varchar(255) NOT NULL COMMENT 'asal sekolah maba',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pendaftaran_mahasiswa`
--

/*!40000 ALTER TABLE `pendaftaran_mahasiswa` DISABLE KEYS */;
INSERT INTO `pendaftaran_mahasiswa` VALUES (1,'yqjrv','xpvpw','dnxps','ekvmg','ywtbc'),(2,'ehedh','lzmpc','tvxfh','yihcx','jorik'),(3,'gwsao','fwzmm','vcjht','ovifh','jamhh'),(4,'wiopd','fcmct','engyd','xcdrm','jebjy'),(5,'dwtli','vhbfg','ogtfd','aywag','kswmm'),(6,'xtoin','mdykv','cdgld','ozcph','bdnfo'),(7,'lwhcq','mipru','tqgcj','exeer','mcrfn'),(8,'gesse','hkdry','taiwp','logxj','fpoqq'),(9,'jjqvx','tjbrf','vyceu','isudi','dcost'),(10,'Dila Arumi','Yogyakarta','perempuan','Islam','SMA 1 Piri Yogyakarta'),(11,'Tono','Mergan','laki-laki','Islam','SMA 1 Malang');
/*!40000 ALTER TABLE `pendaftaran_mahasiswa` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-16  6:27:10
