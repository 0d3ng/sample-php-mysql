# Sample Aplikasi Php MySQL

Contoh kode program menggunakan PHP dan MySQL untuk fungsi CRUD

## Tampilan Aplikasi

Terdiri dari beberapa halaman seperti home, pendaftar, dan halaman pendaftaran

- Halaman Home

  ![Halaman Home](res/view/home.png)

- Halaman Pendaftar

  ![Halaman Pendaftar](res/view/pendaftar.png)

- Halaman Pendaftaran

  ![Halaman Pendaftaran](res/view/pendaftaran.png)

## Resources

Beberapa resources yang dapat digunakan ada di alamat berikut

- [Database](res/_2021-08-16_062651_pendaftaran.sql)
- [PPT](res/Materi-Pembekalan-2021.pptx)
- [Design](res/pendaftaran.drawio)
- [Image collection](res/report)
